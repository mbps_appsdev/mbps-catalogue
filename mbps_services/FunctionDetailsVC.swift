//
//  FunctionDetailsVC.swift
//  mbps_services
//
//  Created by iOS Development on 11/06/2019.
//  Copyright © 2019 Manulife Business Processing Service. All rights reserved.
//

import Foundation
import UIKit



class FunctionDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate, UIWebViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var webViewVideo: UIWebView!
    
    @IBOutlet weak var webView: UIWebView!
    
    @IBOutlet weak var viewContainerWeb: UIView!
    
    @IBOutlet weak var navTittle: UILabel!
    
    @IBOutlet weak var constraintHeightGreenHeader: NSLayoutConstraint!
    
//    @IBOutlet weak var constraintHeightWebView: NSLayoutConstraint!
    
    
    var prevOffset = 0.0
    var heightWithVideo = 0.0
    var functionHeadName: String = ""
    var functionHeadEmail: String = ""
    var functionHeadVideo: String = ""
    var functionHeadPoster: String = ""
    
    var htmlString = """
                        <html>
                        <head>
                        <style>
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }

                            
                        </style>
                        </head>
                        <body>
                        <br/>
                        <strong>Experience Analytics</strong><br/>
                        <p>Analysis of policyholder experience data using statistical and analytical software, recommending assumptions to use in pricing and valuation models</p><br/>
                        <br/>
                        <strong>Liability Modeling</strong><br/>
                        <p>Comprehensive approach to analyzed risk and reward in terms of the overall pension plan market</p><br/>
                        <br/>
                        <strong>Valuation & CALM Support</strong><br/>
                        <p>Various services including production of reserves and other output for financial reporting, analysis and reporting on rseserves/capital for multiple product types and accounting bases, and contstruction of new/enhanced AXIS valuation models</p><br/>
                        <br/>
                        <strong>Advanced Analytics</strong><br/>
                        <p>The team uses statistical models and builds data visualization dashboards to gain business insight to several areas such as fraud, marketing campaigns and NPS</p><br/>
                        <br/>
                        </body>
                        </html>
                        """
    var functiondetails_manilacebu:[Dictionary<String, String>] = [
        Dictionary.init(dictionaryLiteral: ("numbers", "45"), ("location", "Asia")),
        Dictionary.init(dictionaryLiteral: ("numbers", "17"), ("location", "Canada")),
        Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "United States")),
        Dictionary.init(dictionaryLiteral: ("numbers", "45"), ("location", "Group functions")),
        Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "Wealth Asset Management"))]
    
    var functiondetails_chengdu:[Dictionary<String, String>] = [
        Dictionary.init(dictionaryLiteral: ("numbers", "3"), ("location", "Asia")),
        Dictionary.init(dictionaryLiteral: ("numbers", "4"), ("location", "Group functions"))]
    
    var isDetailsShown = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.webView.loadHTMLString(self.htmlString, baseURL: nil)
        
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        
        //        let path:String = Bundle.main.path(forResource: "intro_video", ofType: "mp4")!
        let path:String = Bundle.main.path(forResource: self.functionHeadVideo, ofType: "mp4")!
        var posterPath:String = ""
        
        if(self.functionHeadPoster.count != 0) {
            
            posterPath = Bundle.main.path(forResource: self.functionHeadPoster, ofType: "png")!
        }
        
        let playPath:String = Bundle.main.path(forResource: "play30", ofType: "png")!
        
        //        print("path:\(path)")
        var htmlStringVideo = """
        <html>
        <head>
        <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>
        <style>
        html, body {
        background:#2CB565;
        color: #FFFFFF;
        font-family: 'ManulifeJHSans-Regular'
        }
        h1 {
        color: #2CB565;
        }
        div {
        padding:20px 40px;
        padding-bottom:0px;
        }
        video {
        margin-top:-20px;
        width:calc(100% + 20px);
        margin-left:-10px;
        margin-right:-10px;
        }
        
        </style>
        </head>
        <body>
        <br/>
        <div style="position:relative; width:100%; margin:0; padding:0;">
        <video id="video" src="file://\(path)" poster="file://\(posterPath)" type="video/mp4" controls preload playsinline>
        </video>
        <img id="playbutton" src="file://\(playPath)" style="position:absolute; width:44px height:44px; left:calc(50% - 50px); top:calc(50% - 62px);"/>
        <script type="text/javascript">
        $(document).ready(function(){
        $("img#playbutton").on("click", function(){
        $("#video").get(0).play();
        $("img#playbutton").hide();
        });
        
        $("#video").on("play playing", function(){
        
        $("img#playbutton").hide();
        });
        
        $("#video").on("pause", function(){
        
        $("img#playbutton").show();
        });
        
        });
        </script>
        </div>
        <br/>
        <div>
        <p>Function Head:<br/>\(functionHeadName)</p>
        <p>✉ \(functionHeadEmail)</p>
        </div>
        </body>
        </html>
        """
        
        if(self.functionHeadVideo.count == 0) {
            htmlStringVideo = """
            <html>
            <head>
            <style>
            html, body {
            background:#2CB565;
            color: #FFFFFF;
            font-family: 'ManulifeJHSans-Regular'
            }
            h1 {
            color: #2CB565;
            }
            div {
            padding:20px 40px;
            padding-bottom:0px;
            }
            video {
            margin-top:-20px;
            width:calc(100% + 20px);
            margin-left:-10px;
            margin-right:-10px;
            }
            
            </style>
            </head>
            <body>
            <div>
            <p>Function Head:<br/>\(functionHeadName)</p>
            <p>✉ \(functionHeadEmail)</p>
            </div>
            </body>
            </html>
            """
        }
        
        self.webViewVideo.loadHTMLString(htmlStringVideo, baseURL: nil)
        self.webViewVideo.allowsInlineMediaPlayback = true
        self.webViewVideo.mediaPlaybackRequiresUserAction = false
        
        self.tableView.bounces = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
//        let cssString:String = "body { font-family: 'ManulifeJHSans-Regular'; font-size: 15px; } strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }";
//        let javascriptString:String = "var style = document.createElement('style'); style.innerHTML = '\(cssString)'; document.head.appendChild(style)";
//        
//        webView.stringByEvaluatingJavaScript(from: javascriptString)
        
        
        if(webView.isEqual(self.webView)) {
            self.webView.frame.size.height = 1
            self.viewContainerWeb.frame.size = self.webView.sizeThatFits(CGSize.zero)
            
            if(self.viewContainerWeb.frame.size.height < (self.view.frame.size.height - 100)) {
                self.viewContainerWeb.frame.size = CGSize.init(width: self.viewContainerWeb.frame.size.width, height: self.viewContainerWeb.frame.size.height + (self.view.frame.size.height * 0.5))
            }
            
            self.tableView.tableFooterView = self.viewContainerWeb
            
            self.webView.scrollView.isScrollEnabled = false
            self.webView.scrollView.bounces = false
            self.webView.isUserInteractionEnabled = false
            
            self.tableView.reloadData()
        }
        else {
            self.webViewVideo.frame.size.height = 1
//            self.cons.frame.size = webView.sizeThatFits(CGSize.zero)
//            self.constraintHeightGreenHeader.constant = self.webViewVideo.sizeThatFits(CGSize.zero).height + 100 + 120 /*padding*/
            
            self.heightWithVideo = Double(self.webViewVideo.sizeThatFits(CGSize.zero).height + 100)
            
//            self.constraintHeightWebView.constant = CGFloat(self.webViewVideo.sizeThatFits(CGSize.zero).height)
            
            self.constraintHeightGreenHeader.constant = CGFloat(self.heightWithVideo)
            
            self.webViewVideo.scrollView.isScrollEnabled = false
            self.webViewVideo.scrollView.bounces = false
            
            self.webViewVideo.layoutSubviews()
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if(self.isDetailsShown == false) {
            return 1
        }
        
        if(functiondetails_manilacebu.count == 0 || functiondetails_chengdu.count == 0) {
            return 1
        }
        
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.isDetailsShown == false) {
            return 0
        }
        if(section == 0) {
            return functiondetails_manilacebu.count
        }
        return functiondetails_chengdu.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        var data:Dictionary<String, String>
        if(indexPath.section == 0) {
            data = functiondetails_manilacebu[indexPath.row]
        }
        else {
            data = functiondetails_chengdu[indexPath.row]
        }
        
        let text:String = String("\(data["numbers"]!) \(data["location"]!)")
        
//        let cell:TableViewCellFunctionDetail? = self.tableView.cellForRow(at: indexPath) as? TableViewCellFunctionDetail
//
//        if(cell == nil) {
//            return 72.0
//        }
        
//        let labelWidth = cell!.labelLocation.frame.size.width
        let labelWidth = (self.tableView.frame.size.width * 0.8) - 15.0
        let maxLabelSize = CGSize(width: labelWidth, height: CGFloat.greatestFiniteMagnitude)
        let actualLabelSize = text.boundingRect(with: maxLabelSize, options: [.usesLineFragmentOrigin], attributes: [.font: UIFont.init(name: "ManulifeJHSans-Regular", size: 20.0)!], context: nil)
        let labelHeight = actualLabelSize.height
        
        if(labelHeight > 62.0) {
            return labelHeight
        }
        return 62.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView:TableViewSectionHeader = self.tableView.dequeueReusableCell(withIdentifier: "sectionHeader") as! TableViewSectionHeader
        headerView.buttonChevron.addTarget(self, action: #selector(showHideLocationDetails), for: UIControlEvents.touchUpInside)
//        if(self.isDetailsShown == false) {
//            headerView.labelLocation.text! = "See headcount summary"
//            headerView.labelLocation.font = UIFont.init(name: "Manulife JHSans - Light", size: 20.0)
//            headerView.buttonChevron.isHidden = false
//
//            return headerView;
//        }
        
        if(section == 0) {
            headerView.labelLocation.text! = "MBPS Manila and Cebu"
            headerView.labelLocation.font = UIFont.init(name: "ManulifeJHSans-Demibold", size: 20.0)
            headerView.buttonChevron.isHidden = false
            if(self.isDetailsShown == false) {
                headerView.labelLocation.text! = "See headcount summary"
                headerView.labelLocation.font = UIFont.init(name: "ManulifeJHSans-Light", size: 20.0)

            }
            
            let tap:UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(showHideLocationDetails))
            tap.numberOfTapsRequired = 1
            tap.numberOfTouchesRequired = 1
            
            headerView.addGestureRecognizer(tap)
            
        }
        else {
            headerView.labelLocation.text! = "MBPS Chengdu"
            headerView.labelLocation.font = UIFont.init(name: "ManulifeJHSans-Demibold", size: 20.0)
            headerView.buttonChevron.isHidden = true
            
        }
        return headerView;
    }
    
    @objc func showHideLocationDetails(){
        if(self.isDetailsShown) {
            self.isDetailsShown = false
        }
        else {
            self.isDetailsShown = true
        }
//        for family: String in UIFont.familyNames
//        {
//            print("\(family)")
//            for names: String in UIFont.fontNames(forFamilyName: family)
//            {
//                print("== \(names)")
//            }
//        }
        self.tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCellFunctionDetail = (tableView.dequeueReusableCell(withIdentifier: "cellDetails") as? TableViewCellFunctionDetail)!
        
        var data:Dictionary<String, String>
        if(indexPath.section == 0) {
            data = functiondetails_manilacebu[indexPath.row]
        }
        else {
            data = functiondetails_chengdu[indexPath.row]
        }
        
        cell.labelNumber.text! = data["numbers"]!
        cell.labelLocation.text! = data["location"]!
        
        if(Int(data["numbers"]!) == 0) {
            //hide?
//            cell.contentView.frame = CGRect.zero
//            cell.labelNumber.text! = ""
//            cell.labelLocation.text! = ""
//            cell.labelNumber.frame = CGRect.zero
//            cell.labelLocation.frame = CGRect.zero
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        var scrollDown = false
        
//        print("prev: \(self.prevOffset) - offset: \(scrollView.contentOffset.y)")
        
        if(Float(self.prevOffset) > Float(scrollView.contentOffset.y)) {
            scrollDown = true
        }
        
        var height:CGFloat = 0.0
        
        var change:CGFloat = 10
        
        change = CGFloat(abs(Float(scrollView.contentOffset.y) - Float(self.prevOffset)))
        
        
        if(scrollDown && scrollView.contentOffset.y < 0) {
            
            self.constraintHeightGreenHeader.constant = self.constraintHeightGreenHeader.constant + change
            if(self.constraintHeightGreenHeader.constant >= 100) {
             
                scrollView.setContentOffset(.zero, animated: false)
            }
        }
        else {
            self.constraintHeightGreenHeader.constant = self.constraintHeightGreenHeader.constant - change
            if(self.constraintHeightGreenHeader.constant >= 100) {
                
                scrollView.setContentOffset(.zero, animated: false)
            }
        }
        
        if (self.constraintHeightGreenHeader.constant <= 110.0) {
            self.constraintHeightGreenHeader.constant = 100
        }
        else if(self.constraintHeightGreenHeader.constant >= CGFloat(self.heightWithVideo)) {
            
            self.constraintHeightGreenHeader.constant = CGFloat(self.heightWithVideo)
        }
        
        self.prevOffset = Double(scrollView.contentOffset.y)
        
//        var scrollDown = true
//        if(Float(self.prevOffset) > Float(scrollView.contentOffset.y)) {
//            scrollDown = false
//        }
//
//        var height:CGFloat = 0.0
//
////        if(scrollDown == false) {
////            height = CGFloat(Float(self.heightWithVideo) - (Float(scrollView.contentOffset.y) - Float(self.prevOffset)))
////        }
////        else {
////            height = CGFloat(Float(self.heightWithVideo) + (Float(scrollView.contentOffset.y) - Float(self.prevOffset)))
////        }
//
//        height = CGFloat(Float(self.heightWithVideo) - (Float(scrollView.contentOffset.y) - Float(self.prevOffset)))
//
//
//
//        if(Float(height) >= Float(self.heightWithVideo)) {
//            height = CGFloat(self.heightWithVideo)
//        }
//        else if(height < CGFloat(110.0)) {
//            height = 100.0
//        }
////        else if(scrollDown == true && scrollView.contentOffset.y > 250) {
////            self.constraintHeightGreenHeader.constant = 100.0
////        }
//        else {
////            self.constraintHeightGreenHeader.constant = CGFloat(self.heightWithVideo)
//            print("height: \(height)")
//
//
//        }
//
//        self.constraintHeightGreenHeader.constant = height
        
//        if(scrollView.contentOffset.y < 10) {
//            //show header
//
//            self.constraintHeightGreenHeader.constant = CGFloat(self.heightWithVideo)
//        }
//        else {
//            //hide
//
//            self.constraintHeightGreenHeader.constant = 100.0
//        }
    }
    
    @IBAction func backFunctionsVC(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}



class TableViewCellFunctionDetail: UITableViewCell {
    @IBOutlet weak var labelNumber: UILabel!
    @IBOutlet weak var labelLocation: UILabel!
}


class TableViewSectionHeader: UITableViewCell {
    @IBOutlet weak var labelLocation: UILabel!
    @IBOutlet weak var buttonChevron: UIButton!
}
