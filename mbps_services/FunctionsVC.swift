//
//  FunctionsVC.swift
//  mbps_services
//
//  Created by iOS Development on 10/06/2019.
//  Copyright © 2019 Manulife Business Processing Service. All rights reserved.
//

import UIKit

class FunctionsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableViewFunctions: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    let functions = ["Actuarial and Advanced Analytics", "Administrative Services", "Change Management Services", "Claims", "Contact Center", "Digital Operations", "Finance", "Investments", "Information Systems", "New Busines and Underwriting"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.backButton.layer.cornerRadius = self.backButton.frame.width / 2
        self.backButton.clipsToBounds = true
        
        self.nextButton.layer.cornerRadius = self.nextButton.frame.width / 6
        self.backButton.clipsToBounds = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        self.nextButton.isHidden = true
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return functions.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCellFunctions = (tableView.dequeueReusableCell(withIdentifier: "cellFunctions") as? TableViewCellFunctions)!
        
        cell.labelFunctionName.text! = functions[indexPath.row]
        
        cell.viewAccessoryArrow.layer.cornerRadius = (cell.viewAccessoryArrow.frame.size.width) / 2
        cell.viewAccessoryArrow.clipsToBounds = true
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewFunctions.deselectRow(at: indexPath, animated: true)
        
        let vc:FunctionDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "functionsdetailsvc") as! FunctionDetailsVC
        
        switch functions[indexPath.row] {
        case "Actuarial and Advanced Analytics":
            DispatchQueue.main.async {
                  vc.navTittle.text = "Actuarial and Advanced Analytics"
            }
           vc.functionHeadName = "Carlo Tenedero"
            vc.functionHeadEmail = "Carlo_Tenedero@manulife.com"
            vc.functionHeadVideo = "carlo_video"
            vc.functionHeadPoster = "thumb_carlo_video"
            vc.htmlString = """
                        <html>
                        <head>
                        <style>
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
                        </style>
                        </head>
                        <body>
                        <br/>
                        <strong>Experience Analytics</strong><br/>
                        <p>Analysis of policyholder experience data using statistical and analytical software, recommending assumptions to use in pricing and valuation models</p><br/>
                        <br/>
                        <strong>Liability Modeling</strong><br/>
                        <p>Comprehensive approach to analyzed risk and reward in terms of the overall pension plan market</p><br/>
                        <br/>
                        <strong>Valuation & CALM Support</strong><br/>
                        <p>Various services including production of reserves and other output for financial reporting, analysis and reporting on rseserves/capital for multiple product types and accounting bases, and contstruction of new/enhanced AXIS valuation models</p><br/>
                        <br/>
                        <strong>Advanced Analytics</strong><br/>
                        <p>The team uses statistical models and builds data visualization dashboards to gain business insight to several areas such as fraud, marketing campaigns and NPS</p><br/>
                        <br/>
                        </body>
                        </html>
                        """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "45"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "17"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "45"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "3"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "4"), ("location", "Group functions"))]
        case "New Busines and Underwriting":
            DispatchQueue.main.async {
            vc.navTittle.text = "New Busines and Underwriting"
            }
            vc.functionHeadName = "Ali Muslim"
            vc.functionHeadEmail = "Ali_Muslim@manulife.com"
            vc.functionHeadVideo = "ali_iphone"
            vc.functionHeadPoster = "thumb_ali_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Application Input</strong><br/>
            <p>Responsible for reviewing the accuracy of the requirements from the clients and capturing all pertinent information into the systems based on the established guidelines and state regulations. Initiate requests for outstanding requirements and coordinates with the various New Business teams for the timely fulfillment of the policies or contracts
            </p><br/>
            <br/>
            <strong>Policy Implementation and Amendment</strong><br/>
            <p>Responsible for reviewing the documents and administering changes in a policy or contract as requested by the client
            </p><br/>
            <br/>
            <strong>Policy Issuance</strong><br/>
            <p>Responsible for reviewing all pertinent documents on the policy contract and ensure that all information is accurate and complete before issuance of the policy. This may include issuing of cheques as determined along the process and contract management. They liaise with Case Managers, Coordinators and/or Underwriters to request for missing information
            </p><br/>
            <br/>
            <strong>NB Support</strong><br/>
            <p>Review of additional requirements, such as, but not limited to policy replacements, licensing, compensation and ordering of additional requirements, necessary to issue a policy
            </p><br/>
            <br/>
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "129"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "193"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "223"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "13"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "55"), ("location", "Wealth Asset Management"))]
            
//            vc.functiondetails_chengdu = [
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Asia")),
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Group functions"))]
            
            vc.functiondetails_chengdu = []
            
        case "Claims":
            DispatchQueue.main.async {
            vc.navTittle.text = "Claims"
            }
            vc.functionHeadName = "Olivia Taloma"
            vc.functionHeadEmail = "Olivia_Taloma@manulife.com"
            vc.functionHeadVideo = "olivia_video"
            vc.functionHeadPoster = "thumb_olivia_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Claims Payment</strong><br/>
            <p>Processing, determination of payment resulting to a pay out

            </p><br/>
            <br/>
            <strong>Claims Adjustment</strong><br/>
            <p>Reprocessing of previously paid claims
            </p><br/>
            <br/>
            <strong>Claims Estimate</strong><br/>
            <p>Estimate of benefit only
            </p><br/>
            <br/>
            <strong>Claims Audit</strong><br/>
            <p>Determining accuracy of claims payment
            </p><br/>
            <br/>
            <strong>Claims Verification</strong><br/>
            <p>Confirming the validity of claims payment or eligibility through online reference , phone call interview or updated medical review
            </p><br/>
            <br/>
            <strong>Claims Indexing</strong><br/>
            <p>Indexing
            </p><br/>
            <br/>
            <strong>General Claims Administration</strong><br/>
            <p>All other activities that does not result to a payment, adjustment, estimate, audit or verification: Requests to set up and update client information, terminate claims, update of provider registry, request for waiver of premium (WOP), letters and forms

            </p><br/>
            <br/>
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "46"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "317"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "134"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "14"), ("location", "Wealth Asset Management"))]
            
//            vc.functiondetails_chengdu = [
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Asia")),
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Group functions"))]
            
            vc.functiondetails_chengdu = []
            
        case "Contact Center":
            DispatchQueue.main.async {
            vc.navTittle.text = "Contact Center"
            }
            vc.functionHeadEmail = "Divina_Chavez@manulife.com"
            vc.functionHeadName = "Divina Chavez"
            vc.functionHeadVideo = "divine_iphone"
            vc.functionHeadPoster = "thumb_divine_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Inbound and outbound calls, email and chat support</strong><br/>
            <p>The Contact Center Function provides effortless customer service to Manulife and John Hancock policy holders, financial advisors and plan sponsors in North America and the Philippines. <br/> Our teams answer queries on insurance coverage, wealth products, claims, investment changes, technical and pre-sales support. <br/> Our Trainers, Quality Analysts, Subject Matter Experts, and Senior Customer Service Professionals provide support to our onshore and offshore operations
            </p><br/>
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "87"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "491"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "187"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "32"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "339"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "55"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "1"), ("location", "Group functions"))]
            
        case "Digital Operations" :
            DispatchQueue.main.async {
            vc.navTittle.text = "Digital Operations"
            }
            
            vc.functionHeadEmail = "Iris_D_Yacat@manulife.com"
            vc.functionHeadName = "Iris Yacat"
            vc.functionHeadVideo = "iris_video"
            vc.functionHeadPoster = "thumb_iris_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Graphic Design Services</strong><br /><p>Visual Communication and problem solving through the correct use of typography, space, image, and color</p><br />
            <strong>Sharepoint Development</strong><br /><p>Consultation, development, migration, integration, and customization of SharePoint Online and OnPremise</p><br />
            <strong>User Interface and User Experience</strong><br /><p>Design that leads to enhanced visitor engagement for web and mobile applications</p><br />
            <strong>Forms Creation </strong><br /><p>Simple, Complex, and Smart forms design that are also AODA-compliance</p><br />
            <strong>Video Animation </strong><br /><p>Explainer videos, demo videos, corporate videos, and marketing videos for business</p><br />
            <strong>On-site Filming and Event Coverage </strong><br /><p>Production staff available for on-site coverage with same day edit capability</p><br />
            <strong>Elearning</strong><br /><p>Interactive learning modules</p><br />
            <strong>Copywriting</strong><br /><p>Copywriting and proofreading of marketing materials</p><br />
            <strong>Monitoring</strong><br /> <p>Active surveillance of social media pages (Facebook, Twitter, Instagram, YouTube) analyzing incoming content from clients and users and responding with needed solutions using brand-approriate tone</p> <br />
            <strong>Reporting &amp; Analytics</strong><br /> <p>Analysis of trends on usage of websites, internal sites, and social media pages using measurement tools like Google Analytics, Facebook Insights, Hootsuite, and Brandwatch. Provides insights on user behavior and preferences</p> <br />
            <strong>Content Management</strong><br /> <p>Production of content plan for Social Media, management of website, and social media publishing and Facebook advertising</p> <br />
            <strong>Email Management</strong><br /> <p>Moderation of internal Inbox activities, responding to stakeholder requests and inquiries, or escalating cases to identified POCs</p> <br />
            <strong>Social Listening</strong><br /> <p>Collection and analysis of data from social media and other online sources using Brandwatchand organic search strategies, identifying trends and information critical to crisis monitoring or specific events related to Manulife</p> <br />
            <strong>Anti-Money Laundering (AML) QA</strong> <br /> <p>Responsible for providing assurance that the Business Units&rsquo; AML/ATF and Compliance Programs are in compliance with the Company&rsquo;s Global AML/ATF Policy and Asia Divisional Risk Management Program, applicable compliance policies, and with all applicable local laws and regulations</p><br />
            <strong>AML Project Management (PMO) </strong> <br /> <p>Provides production support including system upgrades and function enhancements, as well as handle other AML-related project initiatives</p><br />
            <strong>Foreign Account Tax Compliance Act (FATCA) and</strong> <br /> <p>Common Reporting Standard (CRS)</p> <br /> <p>Responsible for the quality checking of the submitted documents and ensuring that we are compliant to the government mandate.&nbsp; Provides&nbsp;report on&nbsp;financial information&nbsp;to help combat tax evasion and protect the integrity of tax systems</p><br />
            <strong>AML Enhanced Due Diligence (EDD)</strong> <br /> <p>Provides comprehensive research on high-net worth individuals to ensure that these customers are not at risk for potential money laundering/terrorist financing activities.&nbsp; Conducts third party distributor due diligence for Wealth Asset Management-Asia</p><br />
            <strong>Compliance Testing</strong> <br /> <p>Created to execute independent, detailed compliance review testing of Manulife offices across Asia to assess compliance with regulations and firm policies.&nbsp; Conducts in-depth assessment of the relevant reports and data gathered during testing period and thereafter providing recommendations on the observations found</p><br />
            <strong>Regulatory Reporting</strong> <br /> <p>Provides reporting to business unit, divisional and corporate management, as well as the Board of Directors, to allow them to monitor and oversee Manulife&rsquo;s compliance with regulatory obligations.&nbsp; Facilitates the collection, aggregation, analysis and escalation of compliance matters as the Asia Compliance point persons for the Integrated Approach Program (IAP)</p><br />
            <strong>Learning and Transformation (Training) </strong> <br /> <p>In-charge of delivering training across Asia Division business units to enhance compliance awareness.&nbsp; Acts as the subject matter experts on instructional design and training platforms, namely the Compass LMS</p><br />
            <strong>Data Engineering </strong> <br /> <p>Building data pipelines to pull together information from different source systems, integrating, consolidating, cleansing data, and structuring it for use in reporting and analytics</p><br />
            <strong>Quality engineering </strong> <br /> <p>Drives continuous improvement of the development of high quality products and processes</p><br />
            <strong>Data Visualization and Analysis</strong> <br /> <p>Automated reporting capabilities that empowers business information through use of strong, dynamic, insightful, self-service data visualization</p><br />
            <strong>Reporting</strong> <br /> <p>Manage, integrate and centralize business information by way of standard and targeted business reporting</p><br />
            <strong>Software Applications Development and Systems Support</strong> <br /> <p>A full-cycle innovative enterprise solutions provider specializing in Web and Mobile technology</p><br />
            <strong>Workforce Optimization</strong> <br /> <p>Implements practices&nbsp;that supports strategic workforce planning through&nbsp;forecasting, capacity planning and scheduling</p><br />
            <strong>Innovative Technology Solution</strong> <br /> <p>Delivers automation solutions with the use of robotics software and enabling technologies</p><br />
            <p>Service include:</p><br />
            <p><ul>
            <li>RPA Development – Assessment, design and development/enhancement of automation solutions using robotics software</li>
            <li>RPA Analysis – Business process assessment and future state automation modelling</li>
            <li>Project Support – End-to-end project management and support from development to automation deployment</li>
            <li>Production Support – 24/5 monitoring of deployed automations, incident and escalation management including break fixes and maintenance</li>
            </ul></p><br />
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "16"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "87"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "96"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "26"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "55"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "1"), ("location", "Group functions"))]
        case "Finance":
            DispatchQueue.main.async {
            vc.navTittle.text = "Finance"
            }
            vc.functionHeadEmail = "Pilar_Baltazar@manulife.com"
            vc.functionHeadName = "Pilar Baltazar"
            vc.functionHeadVideo = "pilar_video"
            vc.functionHeadPoster = "thumb_pilar_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Financial Controllership</strong><br /> <p>An end to end solution that ensures all financial closing activities are prepared, posted and reviewed, from preparation of journal entries, reconciliation, balance sheet substantiation up to the creation of financial statements required for both internal financial reporting and external regulatory reporting</p><br />
            <strong>Closing Activities</strong><br /> <p>All activities and accounting procedures undertaken at the end of the month to close out the current posting period.&nbsp;This includes depreciation of assets, reconciling inventory discrepancies, posting billing documents, payroll, accrual of expenses and revenues</p><br />
            <strong>GL / Balance Sheet Substantiation</strong><br /> <p>Preparation of schedules or detailed reports to ensure that the General Ledger balances </strong><br /> <p>are correct and consistent with the supporting Subsidiary Ledger balances or other Operations records</p><br />
            <strong>Inter/Intra-company Transactions (Allocations/Recharges)</strong><br /> <p>This refers to the process of recording or allocating financial transactions between different legal entities within the same parent company or the balancing for journals that involve different groups or segments within the same legal entity</p><br />
            <strong>Intersystem, Suspense/Non-Suspense, Ledger Reconciliations</strong><br /> <p>This refers to the process of validating and reconciling the transactions posted in the company&rsquo;s suspense accounts and ensure that all these transactions are reviewed and are recorded in the appropriate accounts within the allowable period</p><br />
            <strong>Financial Reporting / Financial Statement Preparation</strong><br /> <p>Preparation of financial statements including but not limited to Income Statement, Balance Sheet and Statement of Cash Flows or other financial schedules or reports as needed</strong><br /> <p>by the Business</p><br />
            <strong>Performance to Plan reporting</strong><br /> <p>This refers to the process of comparing the actual financial results versus the approved plans and targets, highlighting major drivers or contributors causing the variance</p><br />
            <strong>Regulatory Reporting</strong><br /> <p>Preparation of financial reports and schedules that are required by local or corporate regulatory bodies or external agencies including auditors</p><br />
            <strong>Management Reporting</strong><br /> <p>Preparation of internal reports and schedules that are used for management&rsquo;s analysis, evaluation of the company strategies and may serve as basis for the management&rsquo;s decision making</p><br />
            <strong>Budgeting, Financial Planning and Analysis</strong><br /> <p>Includes analysis and preparation of forecasts, targets and plans based on the actual historical data or based on the organization&rsquo;s key strategies and plans. This also includes creation of financial models used so business segments and teams can prepare their targets and budgets</p><br />
            <strong>Audit Substantiation Support</strong><br /> <p>Pertains to preparation and providing all the necessary schedules and supporting evidences as needed by internal or external auditors to support the accounting transactions made</p><br />
            <strong>Financial Reporting / Financial Statement Preparation</strong><br /> <p>Preparation of financial statements including but not limited to Income Statement, Balance Sheet and Statement of Cash Flows or other financial schedules or reports as needed</strong><br /> <p>by the Business</p><br />
            <strong>Performance to Plan reporting</strong><br /> <p>This refers to the process of comparing the actual financial results versus the approved plans and targets, highlighting major drivers or contributors causing the variance</p><br />
            <strong>Regulatory Reporting</strong><br /> <p>Preparation of financial reports and schedules that are required by local or corporate regulatory bodies or external agencies including auditors</p><br />
            <strong>Management Reporting</strong><br /> <p>Preparation of internal reports and schedules that are used for management&rsquo;s analysis, evaluation of the company strategies and may serve as basis for the management&rsquo;s decision making</p><br />
            <strong>Budgeting, Financial Planning and Analysis</strong><br /> <p>Includes analysis and preparation of forecasts, targets and plans based on the actual historical data or based on the organization&rsquo;s key strategies and plans. This also includes creation of financial models used so business segments and teams can prepare their targets and budgets</p><br />
            <strong>Audit Substantiation Support</strong><br /> <p>Pertains to preparation and providing all the necessary schedules and supporting evidences as needed by internal or external auditors to support the accounting transactions made</p><br />
            <strong>Market and Competitor Intelligence</strong><br /> <p>Provide market intelligence research for life and asset management industries to spot current and emerging market trends spanning across three levels &ndash; industry or country level, competitor and product research</p><br />
            <strong>Target Due Diligence</strong><br /> <p>Conduct forward reviews of target company acquisitions and partnerships wherein staff assesses a company using several factors such as financial strength and governance to determine its viability for acquisition or partnership</p><br />
            <strong>Management Reporting and Analytics</strong><br /> <p>Provide senior management with business insights on the organization&rsquo;s inter-functional and divisional performance and spend and delivers competitors benchmarking for strategic planning and budgeting. Deals with huge amount of data from different functions such as Marketing, Technology, Procurement and Investor Relations and do the end-to-end process of data scrubbing, data analysis, to dashboard creation and publishing.</p><br />
            <strong>Credit Risk Data and Compliance</strong><br /> <p>Responsible for monitoring of Manulife&rsquo;s investment portfolios with respect to their</strong><br /> <p>limit structures</p><br />
            <strong>Regulatory Reporting</strong><br /> <p>Delivers fund regulatory reporting for funds available for sale under John Hancock Investments brand. Regulatory reports includes Portfolio of Investments Reporting,</strong><br /> <p>form N-PORT and N-CEN</p><br />
            <strong>Treasury Operations</strong><br /> <p>Responsible for receiving, processing and paying invoices.&nbsp; The team also sets-up vendor payment details and prepares bank reconciliation reports.&nbsp;Other areas include performing daily cash management activities and overseeing global compliance to Treasury internal controls.</p><br />
            <strong>Accounting Operations</strong><br /> <p>Responsible for financial planning, forecasting and reporting of operational and project expenses.&nbsp; The team aims to provide relevant expense trending analysis and commentaries through confirmation of expected invoices, review of chargeback / expense allocations, expense accrual and adjusting month end journal entries</p><br /><strong>IFS Business Support</strong> <br /> <p>Responsible for the financial reporting systems and processes that support business and regulatory reporting. The key areas include: (1) application development and business support for ongoing operations (security access, data extracts, audit); (2) implementation of changes to the ledger and other finance systems; and (3) maintaining a controlled, reliable, accessible and stable financial reporting environment</p><br />
            <strong>Business Solutions Support</strong> <br /> <p>Develop wide range of spreadsheet modelling applications and database/web mining tools utilizing SQL and VBA for applications. Includes manipulation and development of automated spreadsheets and database applications through VBA programming.</p><br />
            <strong>Risk Management</strong> <br /> <p>Manage the commissioning and qualification process for EUC development. Develop and ensure compliance with established quality standards and governance. Perform Risk and Controls Assessment (RCA) of End User Computing Tools</p><br />
            <strong>Project Management</strong> <br /> <p>Follow short iterative and incremental development cycles to produce prototypes allowing faster product delivery, early testing of the prototype and continuous integration, create test plans to ensure requirements are captured and executed correctly</p><br />
            <strong>Technology Migration</strong> <br /> <p>Participate in specific technology migration, i.e. Conversion of EUC tools from Essbase<br />to SmartView</p><br />
            <strong>MBPS Intake</strong> <br /> <p>The team focus on&nbsp; providing triage and process support, driving two key benefits:</p><br />
            <strong>Simplified and streamlined process</strong><br />
            <strong>Faster turnaround time (resulting in quicker time to market)</strong><br />
            <strong>Specifically, they&nbsp; assess and segment all incoming requests into two categories: High impact versus Low impact</strong><br />
            <strong>Procurement</strong> <br /> <p>The Sourcing team is key to bridging the gap between Manulife and the market.&nbsp;They are responsible for taking the needs of our internal clients, and translating them into specifications that&nbsp;suppliers can deliver. This, in turn, would enhance business and vendor engagement and relationship.</p><br />
            <p>Core responsibilities include:</p><br />
            <p><ul>
            <li>Lead sourcing activities for low impact requests</li>
            <li>Conduct market assessment via RF(x) – RFI, RFP, RFQ</li>
            <li>Field inquiries from vendors and impacted stakeholders</li>
            <li>Engage vendors in competitive bidding process</li>
            <li>Facilitate vendor selection</li>
            <li>Maintain close collaboration with business units for contract negotiation</li>
            <li>Managed Tools: Buy Smart</li>
            </ul></p><br />
            <strong>Paralegal</strong> <br /> <p>Strategic function help establish and maintain strategic relationships between SPO,<br />BUs and vendors. <br />
            <p>Core functions include:</p><br />
            <p><ul>
            <li>Preparation of Non-disclosure Agreement (NDA) –drafting, review and facilitating signatures (Vendor and Manulife) for NDAs as required</li>
            <li>Contract drafting and review – draft and review various legal documents (MSA, SOW, Addendum, Amendment, etc.) as required by various business units</li>
            <li>Vendor negotiation – red lining of contract clauses</li>
            <li>Reports and Analysis – gather and compile data as required by stakeholders</li>
            <li>Decision Making – ensuring that the right decision is make within the fastest timeline</li>
            </ul></p><br />
            <strong>Contract Management</strong> <br /> <p>The team manages ProContracts-wiz Database, a Manulife&rsquo;s official central repository of all procurement and outsourcing arrangements that supports vendor management as well as the risk governance management process</p><br />
            <strong>Vendor Masterfile Admin</strong> <br /> <p>Maintains the accuracy and integrity of the global vendor master file and manage the <br />vendor onboarding process. Acts as primary point of contact for questions related to<br />vendor master data</p><br />
            <strong>Archer Administration</strong> <br /> <p>Archer is Manulife&rsquo;s Governance &amp; Risk Compliance platform, used by a number of groups, including Compliance and Audit</p><br />
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "67"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "44"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "18"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "115"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "36"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "11"), ("location", "Group functions"))]
            
        case "Investments":
            DispatchQueue.main.async {
            vc.navTittle.text = "Investments"
            }
            vc.functionHeadName = "Jairus Jugan"
            vc.functionHeadEmail = "Jairus_Jugan@manulife.com"
            vc.functionHeadVideo = "jairus_video"
            vc.functionHeadPoster = "thumb_jairus_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Investment Services</strong><br /> <p>Middle to Back Office investment services spanning trade operations, cash management, asset servicing, pricing and valuation, portfolio accounting, reconciliation, and settlements<p><br />
            <strong>Alternative Investment Operations</strong><br /> <p>Administrative, investment operations, and accounting services for alternative investment assets including derivatives, private equity, mortgage, and real estate<p><br />
            <strong>Performance and Analytics</strong><br /> <p>Reporting and analysis of fund performance utilizing data-driven insights<p><br />
            <strong>Data and Pricing</strong><br /> <p>Data management and pricing functions covering various General Fund and Asset Management instruments.<p><br />
            <strong>WAM Finance</strong><br /> <p>Finance solutions for Accounting and Reporting, Financial Planning and Analysis, and Expense Management<p><br /> 
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "2"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "8"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "329"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "1"), ("location", "Group functions"))]
            
        case "Information Systems":
            DispatchQueue.main.async {
            vc.navTittle.text = "Information Systems"
            }
            vc.functionHeadName = "Arcie Nicodemus"
            vc.functionHeadEmail = "Arcie_Nicodemus@manulife.com"
            vc.functionHeadVideo = "arcie_video"
            vc.functionHeadPoster = "thumb_arcie_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Applications Development &amp; Systems Support</strong><br /> <p><br />Process and practice of developing and supporting web or system applications</p><br />
            <strong>Technical Operations</strong><br /> <p><br />All process and services that are both provisioned by an IT staff to their internal or <br />external clients</p><br />
            <strong>Security Operations</strong><br /> <p><br />A process that deals with risk and security issues</p><br />
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "87"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "491"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "187"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "32"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "339"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "55"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "1"), ("location", "Group functions"))]
            
        case "Administrative Services":
            DispatchQueue.main.async {
                vc.navTittle.text = "Administrative Services"
            }
            vc.functionHeadName = "Regina Angela Aquino, Director"
            vc.functionHeadEmail = "Regina_Angela_Aquino@manulife.com"
            vc.functionHeadVideo = "angel_video"
            vc.functionHeadPoster = "thumb_angel_video"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Training Design Services</strong><br/>
            <p>Design learning activities that help team develop skills, behaviors and competencies required to meet strategic business goals</p>
            <br/>
            <strong>System Administration Services</strong><br/>
            <p>Configure and set up policy & Plan administration systems</p>
            <br/>
            <strong>Quality Assurance / Checking - Financial & Non-Financial</strong><br/>
            <p>Review completed transactions based on current standards and procedures and assess the risks involved with the errors committed</p>
            <br/>
            <strong>Bank Account Reconciliation</strong><br/>
            <p>Match personal bank account records with the bank statement received monthly or quarterly and the money leaving an account should match the actual money spent so that the balance matches at the end of a particular period</p>
            <br/>
            <strong>Data File Administration</strong><br/>
            <p>Fix data files (in MS Excel Format) received from clients into a recognizable format prior to entering into the system</p>
            <br/>
            <strong>Sales Administration Support</strong><br/>
            <p>Provide administration support to the business unit’s sales team such as data integration, distributions processing and plan contract review reports</p>
            <br/>
            <strong>AML and Fraud Investigation Services</strong><br/>
            <p>Monitor and report unusual bank / financial transactions that could indicate money laundering or fraudulent activities</p>
            <br/>
            <strong>Deposit and Disbursement Management Services</strong><br/>
            <p>Handle incoming and outgoing deposits for Manulife at the individual and group level</p>
            <br/>
            <strong>Reconciliation & Settlement Services (Mutual Funds)</strong><br/>
            <p>An end to end solution that matches transactions from multiple sources, manages exceptions, resolves conflicts, generates file reports and uploads files</p>
            <br/>
            <strong>Incoming, Internal and Outgoing Transfers</strong><br/>
            <p>Handle incoming and outgoing client transfers from and to Manulife or other financial institutions at the individual level</p>
            <br/>
            <strong>Account Management Services</strong><br/>
            <p>Handle policy or plan setup, maintenance, administration and discontinuation</p>
            <br/>
            <strong>Indexing</strong><br/>
            <p>Re-route documents received via fax, e-mail or mailed files to the appropriate business area</p>
            <br/>
            <strong>Licensing & Compensation - Agency Support Service</strong><br/>
            <p>Services that support agents and distributors</p>
            <br/>
            <strong>Corporate Actions</strong><br/>
            <p>Set up of events, review and validation of tender instructions, tax impact assessment, instruct tender and event pay out and related account reconciliation</p>
            <br/>
            <strong>Settlements</strong><br/>
            <p>Match trades, release pending trades, CDS reconciliation and closing and DT (US based) settlement</p>
            <br/>
            <strong>DRIP (Dividend Re-Investment Plan) and Entitlements</strong><br/>
            <p>Fulfillment of all entitlements which include dividends and interest and other related processing activities</p>
            <br/>
            <strong>Credit</strong><br/>
            <p>Administer credit policies and procedures within the dealer</p>
            <br/>
            <strong>Taxation</strong><br/>
            <p>Process and balance remittances to CRA, RQ and IRS, maintain client records in accordance with QI and FATCA standards, prepare tax receipts and ensure adherence to tax reporting deadlines</p>
            <br/>
            <strong>Cash Management Services</strong><br/>
            <p>Process incoming and outgoing funds, wires, electronic  payments, loan deposit, cheque deposits stops and cancels, calculate and initiate inter-company wire transfers and bank-related activities</p>
            <br/>
            <strong>Mortgage Services</strong><br/>
            <p>Appraise and underwrite property, fund loan and discharge mortgaged property</p>
            <br/>
            <strong>Information Security and Risk Management</strong><br/>
            <p>Perform audit coordination and information security services for commercial office</p>
            <br/>




            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "136"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "454"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "391"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "52"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "550"), ("location", "Wealth Asset Management"))]
            
            vc.functiondetails_chengdu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "24"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "42"), ("location", "Group functions"))]
            
        case "Change Management Services":
            DispatchQueue.main.async {
                vc.navTittle.text = "Change Management Services"
            }
            vc.functionHeadName = "Pilar Baltazar, AVP\nElizabeth Dayao, Director"
            vc.functionHeadEmail = "Pilar_Baltazar@manulife.com\nElizabeth_Dayao@manulife.com"
            vc.htmlString = """
            <html>
            <head>
            <style>
            
                            html, body {
                                background:#FFFFFF;
                                color: #34384b;
                                font-family: 'ManulifeJHSans-Regular'
                                padding:20px 40px;
                            }
                            body { font-family: 'ManulifeJHSans-Regular'; font-size: 14px; }
                            strong { font-family: 'ManulifeJHSans-Demibold'; font-size: 20px; }
            
            
            </style>
            </head>
            <body>
            <br/>
            <strong>Applications Development &amp; Systems Support</strong><br /> <p><br />Process and practice of developing and supporting web or system applications</p><br />
            <strong>Requirement Life Cycle</strong><br />
            <p><ul>
            <li>Gather user requirements and build business requirements documentation needed to initiate the development and implementation of business and process solutions.</li>
            <li>Create user stories and build product backlogs needed when running Agile projects</li>
            <li>Design data model, blueprint and design architecture in order to illustrate the impact of change to the overall data management system of the process</li>
            <li>Build future state business or process models and value stream maps that illustrate the desired impact of the change</li>
            </ul>
            </p>
            <br/>
            <strong>User Acceptance Testing (UAT)</strong><br />
            <p><ul>
            <li>Convert user requirements or user stories into test cases depending on the project methodology</li>
            <li>Design test plans, test strategies and test scenarios aligned to the release planning activity.</li>
            <li>Execute testing both for UAT and Agile in-sprint quality testing</li>
            <li>Help create the data requirements in testing and review the effectiveness of test environments</li>
            <li>Create and deploy the defect management system to control defect rates and prevent defect leakages</li>
            </ul>
            </p>
            <br/>
            <strong>Process Excellence</strong><br />
            <p><ul>
            <li>Implement varying methodologies, like Six Sigma, Lean, Kaizen, Business Process Management System, to assess and plan process improvement initiatives</li>
            <li>Run operational and risk assessments in order to filter and prioritize opportunities for process enhancement</li>
            <li>Help define baseline production requirements, like quality and speed drivers that measures process and people performance</li>
            <li>Implement remediation plans, system migration, system enhancement, and solution design as needed</li>
            </ul>
            </p>
            <br/>
            <strong>Project Life Cycle</strong><br />
            <p><ul>
            <li>Build the business case and define the resourcing needed to deploy projects</li>
            <li>Create plans covering, stakeholder management, change communication, cost and time critical to the project execution</li>
            <li>Validate risks and set up controls to avoid delays or additional cost to the project</li>
            <li>Build the transition or cutover plan as the project moves into &quot;business as usual&quot;</li>
            <li>Manage the warranty period and improvement requirements based on initial project deployment feedback</li>
            <li>Run training and build process documents or guide to help manage the change impact</li>
            </ul>
            </p>
            <br/>
            <strong>Architecture or Solutions Planning</strong><br />
            <p><ul>
            <li>Perform business impact analysis and assess action plans prior to development and implementation</li>
            <li>Perform varying methodologies of root cause analysis and prioritize solutions recommendation</li>
            <li>Review end-to-end processes and granular process metrics in order to identify the right area to implement process improvement initiatives</li>
            <li>Perform process re-design to illustrate and provide a common view of the desired change to all stakeholders and members of the project team</li>
            </ul>
            </p>
            <br/>



            
            <br/>
            
            </body>
            </html>
            """
            
            vc.functiondetails_manilacebu = [
                Dictionary.init(dictionaryLiteral: ("numbers", "60"), ("location", "Asia")),
                Dictionary.init(dictionaryLiteral: ("numbers", "23"), ("location", "Canada")),
                Dictionary.init(dictionaryLiteral: ("numbers", "5"), ("location", "United States")),
                Dictionary.init(dictionaryLiteral: ("numbers", "3"), ("location", "Group functions")),
                Dictionary.init(dictionaryLiteral: ("numbers", "7"), ("location", "Wealth Asset Management"))]
            
//            vc.functiondetails_chengdu = [
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Asia")),
//                Dictionary.init(dictionaryLiteral: ("numbers", "0"), ("location", "Group functions"))]
            
            vc.functiondetails_chengdu = []
            
        default:
            break
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    
    @IBAction func backShowVideo(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func backShowContacts(_ sender: Any) {
    }
    
}

class TableViewCellFunctions: UITableViewCell {
    @IBOutlet weak var labelFunctionName: UILabel!
    @IBOutlet weak var viewAccessoryArrow: UIView!
}

