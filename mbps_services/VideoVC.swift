//
//  VideoVC.swift
//  mbps_services
//
//  Created by iOS Development on 10/06/2019.
//  Copyright © 2019 Manulife Business Processing Service. All rights reserved.
//

import Foundation
import UIKit

//https://www.youtube.com/watch?v=B-tEdTkyLD0


class VideoVC: UIViewController, UIWebViewDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    @IBOutlet weak var swipeButton: UIButton!
    
    @IBOutlet weak var labelWelcome: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        self.webView.loadRequest(URLRequest.init(url: URL.init(string: "https://www.youtube.com/watch?v=B-tEdTkyLD0")!))
        
//        UIDevice.current.name
//        let myAttribute = [NSAttributedStringKey.font: UIFont(name: "ManulifeJHSans-Light", size: 32.0)!]
//        let myString = NSAttributedString.init(string: "Welcome to Manulife", attributes: myAttribute)
//
////            NSMutableAttributedString(string: "Welcome to Manulife", attributes: myAttribute )
//
//        let myAttributeName = [NSAttributedStringKey.font: UIFont(name: "ManulifeJHSerif-Italic", size: 69.0)!]
//        let nameString = NSAttributedString.init(string: "\n\n\(UIDevice.current.name)", attributes: myAttributeName)
        
//        let string = NSMutableAttributedString.init(string: "Welcome to Manulife,\n\n\(UIDevice.current.name)")
//        string.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "ManulifeJHSans-Light", size: 32.0)!, range: NSRange.init(location: 0, length: 21))
//        string.addAttribute(NSAttributedStringKey.font, value: UIFont(name: "ManulifeJHSerif-Italic", size: 69.0)!, range: NSRange.init(location: 20, length: "Welcome to Manulife,\n\n\(UIDevice.current.name)".count - 20))
        
        var fullNameArr = String(UIDevice.current.name).components(separatedBy: " ")
        let firstName: String = fullNameArr[0]
        
        self.labelWelcome.text = "Rahul!"//String("\(firstName.replacingOccurrences(of: "’s", with: ""))!")
        self.labelWelcome.numberOfLines = 0
        
        
//        print("\n name: \(firstName.replacingOccurrences(of: "\'", with: ""))")
        
        self.scrollView.contentSize = CGSize.init(width: self.scrollView.frame.size.width * 1.3, height: 0.0)
        self.scrollView.bounces = true;
        
        self.backButton.layer.cornerRadius = self.backButton.frame.width / 2
        self.backButton.clipsToBounds = true
        
        self.nextButton.layer.cornerRadius = self.nextButton.frame.width / 6
        self.backButton.clipsToBounds = true
        
        self.swipeButton.layer.borderColor = UIColor.white.cgColor
        self.swipeButton.layer.borderWidth = 1.0
        self.swipeButton.layer.cornerRadius = self.swipeButton.frame.width / 6
        self.swipeButton.clipsToBounds = true
        loadWeb()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func loadWeb(){
        
        //width="560" height="315"
        
        
        //        let path:String = Bundle.main.path(forResource: "MBPS_Video_v2", ofType: "mp4")!
        let path:String = Bundle.main.path(forResource: "mbps_video_iphone_v4", ofType: "mp4")!
        let posterPath:String = Bundle.main.path(forResource: "thumb_mbps_video", ofType: "png")!
        
        let phebepath:String = Bundle.main.path(forResource: "phebe_video", ofType: "mp4")!
        let phebeposterPath:String = Bundle.main.path(forResource: "thumb_phebe_video", ofType: "png")!
        
        let anyapath:String = Bundle.main.path(forResource: "anya_video", ofType: "mp4")!
        let anyaposterPath:String = Bundle.main.path(forResource: "thumb_anya_video", ofType: "png")!
        
        let playPath:String = Bundle.main.path(forResource: "play30", ofType: "png")!
        
        //        print("path:\(path)")
        let htmlString = """
        <html>
        <head>
        <style>
        html, body {
        background:#34384b;
        color: #FFFFFF;
        font-family: 'ManulifeJHSans-Regular'
        }
        h1 {
        color: #2CB565;
        }
        div {
        padding:20px 40px;
        }
        video {
        width:calc(100% + 20px);
        margin-left:-10px;
        margin-right:-10px;
        }
        </style>
        </style>
        <script
        src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8="
        crossorigin="anonymous"></script>
        </head>
        <body>
        <br/>
        <div style="position:relative; width:100%; margin:0; padding:0; margin-top:10%;">
        <video id="video1" src="file://\(path)" poster="file://\(posterPath)" type="video/mp4" controls="true" playsinline="true">
        </video>
        <img id="playbutton1" src="file://\(playPath)" style="position:absolute; width:44px height:44px; left:calc(50% - 50px); top:calc(50% - 53px);"/>
        <script type="text/javascript">
        $(document).ready(function(){
        $("img#playbutton1").on("click", function(){
        $("#video1").get(0).play();
        $("img#playbutton1").hide();
        });
        
        $("#video1").on("play playing", function(){
        
        $("img#playbutton1").hide();
        });
        
        $("#video1").on("pause", function(){
        
        $("img#playbutton1").show();
        });
        
        });
        </script>
        </div>
        <br/>
        <div>
        <h1>Manulife Business Processing Services</h1>
        <p>MBPS is the global shared service centre of Manulife which provides administrative, finance, investments, marketing, underwriting, actuarial, and information technology services to Manulife and John Hancock operations in Canada, the United States, and Asia.  It was established in the Philippines in April 2006 and operates 24x7 in its main site at the UP North Science and Technology Park in Quezon City, its secondary site in Chengdu, China and its third site in Mactan Newtown, Cebu.   MBPS has over 6,000 employees in all sites and is catering to 300+ business units.</p>
        </div>
        <br/>
        <h1>MBPS Cebu</h1>
        <br/>
        <div style="position:relative; width:100%; margin:0; padding:0; margin-top:10%;">
        <video id="video2" src="file://\(phebepath)" poster="file://\(phebeposterPath)" type="video/mp4" controls="true" playsinline="true">
        </video>
        <img id="playbutton2" src="file://\(playPath)" style="position:absolute; width:44px height:44px; left:calc(50% - 50px); top:calc(50% - 53px);"/>
        <script type="text/javascript">
        $(document).ready(function(){
        $("img#playbutton2").on("click", function(){
        $("#video2").get(0).play();
        $("img#playbutton2").hide();
        });
        
        $("#video2").on("play playing", function(){
        
        $("img#playbutton2").hide();
        });
        
        $("#video2").on("pause", function(){
        
        $("img#playbutton2").show();
        });
        
        });
        </script>
        </div>
        <div>
        <br/>
        <p>Phebe Luis-Vitales, Cebu Site Head</p>
        </div>
        <br/>
        <h1>MBPS Chengdu</h1>
        <br/>
        <div style="position:relative; width:100%; margin:0; padding:0; margin-top:10%;">
        <video id="video3" src="file://\(anyapath)" poster="file://\(anyaposterPath)" type="video/mp4" controls="true" playsinline="true">
        </video>
        <img id="playbutton3" src="file://\(playPath)" style="position:absolute; width:44px height:44px; left:calc(50% - 50px); top:calc(50% - 53px);"/>
        <script type="text/javascript">
        $(document).ready(function(){
        $("img#playbutton3").on("click", function(){
        $("#video3").get(0).play();
        $("img#playbutton3").hide();
        });
        
        $("#video3").on("play playing", function(){
        
        $("img#playbutton3").hide();
        });
        
        $("#video3").on("pause", function(){
        
        $("img#playbutton3").show();
        });
        
        });
        </script>
        </div>
        <br/>
        <div>
        <p>Anya Liao, Chengdu Site Head</p>
        </div>
        </body>
        </html>
        """
        
        self.webView.loadHTMLString(htmlString, baseURL: nil)
        self.webView.allowsInlineMediaPlayback = true
        self.webView.mediaPlaybackRequiresUserAction = false
        
        //        self.webView.loadRequest(URLRequest.init(url: URL.init(string: path)!))
        
        self.webView.scrollView.contentSize = CGSize.init(width: 0.0, height: self.webView.scrollView.contentSize.height)
        
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        if(self.scrollView.contentOffset.x <= self.scrollView.frame.width/8) {
//            self.scrollView.contentOffset.x = self.scrollView.frame.width
//        }
        var offset:CGFloat = self.scrollView.contentOffset.x
        
        offset = abs(offset)
        
        var alpha:CGFloat = 1.0
        
        alpha = 1.0 - (offset / (self.scrollView.frame.width / 5.0))
        
//        self.scrollView.tag = 0
      
        if(alpha < 0.1) {
            self.scrollView.tag = 1
            
            self.scrollView.isHidden = true
            self.scrollView.alpha = 0.0
        }
        else  {
            if(self.scrollView.tag == 1) {
                return
            }
            
            self.scrollView.isHidden = false
            self.scrollView.alpha = CGFloat(alpha)
        }
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        
        if(self.scrollView.alpha < 0.1) {
            
            
            self.scrollView.isHidden = true
            self.scrollView.alpha = 0.0
            
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
//        self.webView.reload()
        loadWeb()
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        loadWeb()
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func backShowWelcome(_ sender: Any) {
//        self.scrollView.alpha = 1.0
//        self.scrollView.contentOffset = CGPoint.zero
//        self.scrollView.isHidden = false;
        
        
        UIView.animate(withDuration: 0.5, animations: {
            self.scrollView.alpha = 1.0
            self.scrollView.contentOffset = CGPoint.zero
            self.scrollView.isHidden = false;
            self.scrollView.tag = 0
        })
        
    }
    
    @IBAction func nextShowFunctions(_ sender: Any) {
        //quickfacts pala
        
        let vc:QuickFactsVC = self.storyboard?.instantiateViewController(withIdentifier: "quickfactsvc") as! QuickFactsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
    @IBAction func swipeButtonPressed(_ sender: Any) {
        
        UIView.animate(withDuration: 1.5, animations: {
            self.scrollView.contentOffset = CGPoint.init(x: self.scrollView.frame.size.width / 5.0, y: 0.0)
        })
        
//        UIView.animate(withDuration: 0.5, animations: {
//            self.scrollView.alpha = 0.0
//            self.scrollView.contentOffset = CGPoint.init(x: self.scrollView.frame.size.width / 5.0, y: 0.0)
//            self.scrollView.isHidden = true;
//            self.scrollView.tag = 1
//        })
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {

//        let ac = UIAlertController(title: "FAILED LOADING", message: nil, preferredStyle: .alert)
//        ac.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
//        present(ac, animated: true, completion: nil)
        
    }
    
}

