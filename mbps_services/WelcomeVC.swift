//
//  WelcomeVC.swift
//  mbps_services
//
//  Created by iOS Development on 10/06/2019.
//  Copyright © 2019 Manulife Business Processing Service. All rights reserved.
//

import Foundation
import UIKit

class WelcomeVC: UIViewController, UIScrollViewDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    
//    @IBOutlet var swipeGesture: UISwipeGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        swipeGesture.direction = .left;
//        swipeGesture.addTarget(self, action: #selector(handleSwipe(_:)))
        
        self.scrollView.contentSize = CGSize.init(width: self.view.frame.size.width * 2.0, height: 0.0)
        self.scrollView.bounces = true;
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("log scroll")
    }
    
//    @objc func handleSwipe(_ sender: UISwipeGestureRecognizer) {
//        let vc: FunctionsVC = self.storyboard?.instantiateViewController(withIdentifier: "functionsvc") as! FunctionsVC
//        self.present(vc, animated: true) {
//
//        };
//    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
