//
//  QuickFactsVC.swift
//  mbps_services
//
//  Created by iOS Development on 10/06/2019.
//  Copyright © 2019 Manulife Business Processing Service. All rights reserved.
//

import Foundation
import UIKit



class QuickFactsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    
    
    let quickfacts:[Dictionary<String, String>] = [
        Dictionary.init(dictionaryLiteral: ("image", "family30"), ("greenText", "6,000+"), ("blackText", "Employees")),
        Dictionary.init(dictionaryLiteral: ("image", "building30"), ("greenText", "3"), ("blackText", "Geographic locations, Manila, Cebu, and Chengdu")),
        Dictionary.init(dictionaryLiteral: ("image", "handshake30"), ("greenText", "300+"), ("blackText", "Business units")),
        Dictionary.init(dictionaryLiteral: ("image", "pay30"), ("greenText", "$250M+"), ("blackText", "Annual cost save"))]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.backButton.layer.cornerRadius = self.backButton.frame.width / 2
        self.backButton.clipsToBounds = true
        
        self.nextButton.layer.cornerRadius = self.nextButton.frame.width / 6
        self.backButton.clipsToBounds = true
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quickfacts.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 195.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:TableViewCellQuickFacts = (tableView.dequeueReusableCell(withIdentifier: "cellFunctions") as? TableViewCellQuickFacts)!
        
        let data:Dictionary<String, String> = quickfacts[indexPath.row]
        
        cell.imageIcon.image = UIImage.init(named: data["image"]!)
        cell.labelGreenText.text = data["greenText"]!
        cell.labelBlackText.text = data["blackText"]!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableView.deselectRow(at: indexPath, animated: true)
    }
    
    @IBAction func backShowVideo(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nextShowFunctions(_ sender: Any) {
        
        let vc:FunctionsVC = self.storyboard?.instantiateViewController(withIdentifier: "functionsvc") as! FunctionsVC
        
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }
    
    
}


class TableViewCellQuickFacts: UITableViewCell {
    @IBOutlet weak var imageIcon: UIImageView!
    @IBOutlet weak var labelGreenText: UILabel!
    @IBOutlet weak var labelBlackText: UILabel!
}
